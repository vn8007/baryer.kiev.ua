// faq block start

try{
    const openBtnSvg = `<svg  class="btn-svg" width="11" height="7" viewBox="0 0 11 7" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M10 1L5.5 5L1 1" stroke-width="2"/>
</svg>`;
    const closedBtnSvg = `<svg  class="btn-svg" width="11" height="7" viewBox="0 0 11 7" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M10 6L5.5 2L1 6" stroke-width="2"/>
</svg>`;
    const btnPlace = document.getElementsByClassName('btn_faq');
    const svgInsertToDiv = () => {
        for(let i=0; i < btnPlace.length; i++ ){
            btnPlace[i].innerHTML = openBtnSvg}
    };
    svgInsertToDiv();

    const faqBlock = document.getElementsByClassName('faq_block');

    const idToElem = () =>{
        for (let i = 0; i < faqBlock.length; i++) {
            faqBlock[i].id = i;
            faqBlock[i].addEventListener('click', open)
            for (let j = 0; j <faqBlock[i].children.length; j++){
                if(faqBlock[i].children[j].classList.contains('qst_block')){
                    for (let k = 0; k < faqBlock[i].children[j].children.length; k++ ){
                        if(faqBlock[i].children[j].children[k].classList.contains('btn_faq')){
                            faqBlock[i].children[j].children[k].id = `btn${i}`;
                        }
                    }
                }
                if(faqBlock[i].children[j].classList.contains('box')){
                    faqBlock[i].children[j].id = `box${i}`
                }
            }
        }
    };
    function closed(e){
        const close = document.getElementById(`close${this.dataset.id}`);
        if(close)
        {
            e.stopPropagation();
            const elem = document.getElementById(`${this.dataset.id}`);
            const btn = document.getElementById(`close${this.dataset.id}`);
            const box = document.getElementById(`box${this.dataset.id}`);
            box.classList.add('invisible');
            elem.classList.add('hover_style');
            btn.innerHTML = openBtnSvg;
            btn.id = `btn${this.dataset.id}`;
            btn.classList.remove('btn_closed');
        }
    }
    function open() {
        const elem = document.getElementById(`${this.id}`);
        const btn = document.getElementById(`btn${this.id}`);
        const box = document.getElementById(`box${this.id}`);
        box.classList.remove('invisible');
        elem.classList.remove('hover_style');
        btn.innerHTML = closedBtnSvg;
        btn.classList.add('btn_closed');
        btn.id = `close${this.id}`;
        btn.dataset.id = `${this.id}`;
        const close = document.getElementById(`close${this.id}`);
        close.addEventListener('click', closed);
    }


    idToElem();
}
catch (e){
    console.log(e)
}
// faq block end


//selector block start
try {
   const titleSelector = document.getElementsByClassName('selector_text')
   const imgsSelector = document.getElementsByClassName('selector_img');
   const textSelector = document.getElementsByClassName('selector_content_text')
   const circleSelector = document.getElementsByClassName('circle_selector');
   const circleSelectorActiveSVG = `<svg class="circle_selector_active" width="43" height="44" viewBox="0 0 43 44" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M40.5 22C40.5 32.824 31.9395 41.5 21.5 41.5C11.0605 41.5 2.5 32.824 2.5 22C2.5 11.176 11.0605 2.5 21.5 2.5C31.9395 2.5 40.5 11.176 40.5 22Z" stroke="url(#paint0_radial_253_3371)" stroke-width="5"/>
        <ellipse cx="21.5" cy="22" rx="13.6818" ry="14" fill="url(#paint1_radial_253_3371)"/>
        <defs>
          <radialGradient id="paint0_radial_253_3371" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(1.61561 5.28) rotate(40.7054) scale(52.6243 92.538)">
            <stop stop-color="#434B57"/>
            <stop offset="1" stop-color="#0F1217"/>
          </radialGradient>
          <radialGradient id="paint1_radial_253_3371" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(8.84629 11.36) rotate(40.7054) scale(33.4882 58.8878)">
            <stop stop-color="#434B57"/>
            <stop offset="1" stop-color="#0F1217"/>
          </radialGradient>
        </defs>
      </svg>`;

    const idToElemSelector = () => {
            for (let i = 0; i < imgsSelector.length; i++) {
                imgsSelector[i].id = 'imgsSelector' + i;
                imgsSelector[i].classList.add(`selector_img_${i+1}`)
            }
            for (let i = 0; i < textSelector.length; i++) {
                textSelector[i].dataset.id = i;
                textSelector[i].id = 'textSelector' + i;
            }
            for (let i = 1; i < textSelector.length; i++){
                textSelector[i].classList.add('selector_invisible')
            }
            for(let i = 0; i < titleSelector.length; i++){
                titleSelector[i].id = 'titleSelector'+i;
                titleSelector[0].classList.add('selector_text_active');
            }
            for (let i = 0; i < circleSelector.length; i++){
                circleSelector[i].dataset.id =  i;
                circleSelector[i].id = 'circleSelector'+i;
                circleSelector[0].innerHTML = circleSelectorActiveSVG;
                circleSelector[0].classList.remove(`circle_selector_act`);
                circleSelector[i].addEventListener('click', setActiveSel)
            }
        }
    idToElemSelector();
    function activeSel(data){
                for (let i = 0; i < textSelector.length; i++) {
                    textSelector[i].classList.add('selector_invisible')
                }
                const textSelectorActive = document.getElementById(`textSelector${data}`);
                textSelectorActive.classList.remove('selector_invisible');
                for (let i = 0; i < titleSelector.length; i++) {
                    titleSelector[i].classList.remove('selector_text_active');
                }
                const titleSelectorActive = document.getElementById(`titleSelector${data}`);
                titleSelectorActive.classList.add('selector_text_active');

                const imgsSelectorActive = document.getElementById(`imgsSelector${data}`);

                for (let i = 0; i < imgsSelector.length; i++) {
                    imgsSelector[i].style.zIndex = 0;
                    imgsSelector[i].style.filter = `blur(3px)`;
                }
                imgsSelectorActive.style.zIndex = 1;
                imgsSelectorActive.style.filter = `blur(0px)`;
                // imgsSelectorActive.style.transition = `all .7s`

                const numberBlock = document.getElementById(`navi_block`);
                numberBlock.innerText=`${Number(data) + 1}/3`;

                const circleSelectorActive = document.getElementById(`circleSelector${data}`);
                for (let i = 0; i < circleSelector.length; i++){
                    circleSelector[i].innerHTML = ``;
                    circleSelector[i].classList.add('circle_selector_act');
                }
                circleSelectorActive.innerHTML = circleSelectorActiveSVG;
                circleSelectorActive.classList.remove('circle_selector_act');
            }

        function setActiveSel(e) {
                const active = document.getElementById(`circleSelector${this.dataset.id}`);
                activeSel(active.dataset.id)
            }

        function stepLeft(){
                const step = Number(document.getElementById('navi_block').textContent[0]);
                if(step===1){
                    activeSel(0)
                }
                else {
                    activeSel(step - 2);
                }
            }

        const leftArrow = document.getElementById('selectLeft');
        leftArrow.addEventListener('click',stepLeft);

        function stepRight(){
                const step = Number(document.getElementById('navi_block').textContent[0]);
                if(step===3){
                    activeSel(2)
                }
                else {
                    activeSel(step);
                }
            }

        const rightArrow = document.getElementById('selectRight');
        rightArrow.addEventListener('click',stepRight);
}
catch (e){
    console.log(e)
}
//selector block end


//selector mobile block start
try {
    const imgsSelectorMobile = document.getElementsByClassName('selector_img_mobile');
    const textSelectorMobile = document.getElementsByClassName('selector_content_text_mobile')

    const idToSelectorMobItems =() => {
        for(let i = 0; i < imgsSelectorMobile.length; i++){
            imgsSelectorMobile[i].id = 'imgsSelectorMobile' + i;
            imgsSelectorMobile[i].classList.add('selector_invisible');
            imgsSelectorMobile[0].classList.remove('selector_invisible');
        }
        for(let i = 0; i < textSelectorMobile.length; i++){
            textSelectorMobile[i].id= 'textSelectorMobile' + i;
            textSelectorMobile[i].classList.add('selector_invisible');
            textSelectorMobile[0].classList.remove('selector_invisible');
        }
    }
    idToSelectorMobItems();

    const activeSelMobile = (data) =>{
        for (let i = 0; i < imgsSelectorMobile.length; i++){
            imgsSelectorMobile[i].classList.add('selector_invisible');
        }
        const activeImgsSelectorMobile = document.getElementById(`imgsSelectorMobile${data}`);
        activeImgsSelectorMobile.classList.remove('selector_invisible');

        for (let i = 0; i < textSelectorMobile.length; i++){
            textSelectorMobile[i].classList.add('selector_invisible');
        }
        const activeTextSelectorMobile = document.getElementById(`textSelectorMobile${data}`)
        activeTextSelectorMobile.classList.remove('selector_invisible');

        const numberBlockMobile = document.getElementById(`navi_block_mobile`);
        numberBlockMobile.innerText=`${Number(data) + 1}/3`;

    }
    function stepLeftMobile(){
        const step = Number(document.getElementById('navi_block_mobile').textContent[0]);
        if(step===1){
            activeSelMobile(0)
        }
        else {
            activeSelMobile(step - 2);
        }
    }
    function stepRightMobile(){
        const step = Number(document.getElementById('navi_block_mobile').textContent[0]);

        if(step===3){
            activeSelMobile(2)
        }
        else {
            activeSelMobile(step);
        }
    }
    const leftArrowMobile = document.getElementById('selectLeft_mobile');
    leftArrowMobile.addEventListener('click',stepLeftMobile);
    const rightArrowMobile = document.getElementById('selectRight_mobile');
    rightArrowMobile.addEventListener('click',stepRightMobile);


}
catch (e){
    console.log(e)
}
//selector mobile block end


//btn-up block start
try{
  const btnUp = document.getElementById('button-up');
  const scrollUp = () => {
    if (document.documentElement.scrollTop > 300) {
      btnUp.classList.remove('show');
    } else {
      btnUp.classList.add('show')
    }
  }

  const scrollingUp = () => {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  btnUp.addEventListener('click', scrollingUp );
  window.addEventListener('scroll', scrollUp);
}
catch (e){
  console.log(e)
}
//btn-up block end


//stick_menu block start
try{
  const stickMenu = document.getElementById('stick_menu');
  const header = document.getElementById('header_main');

  const scrollMenu = () => {
    stickMenu.style.width = `${header.offsetWidth}px`;
    if (document.documentElement.scrollTop > header.offsetHeight) {
      stickMenu.classList.add('_active-menu');
    }
    else {
        stickMenu.classList.remove('_active-menu');
    }
  }
  window.addEventListener('scroll', scrollMenu)
}
catch (e){
  console.log(e)
}
//stick_menu block end


//anim_scroll block start
const animItems = document.querySelectorAll('.anim-items');

if (animItems.length > 0){
    window.addEventListener('scroll', animOnScroll)
    function animOnScroll(params){
        for (let i = 0; i < animItems.length; i++){
            const animItem = animItems[i];
            const animItemHeight = animItem.offsetHeight;
            const animItemOffset = offset(animItem).top;
            const animStart = 10;

            let animItemPoint = window.innerHeight - animItemHeight / animStart;
            if (animItemHeight > window.innerHeight){
                animItemPoint = window.innerHeight - window.innerHeight / animStart;
            }
            if((pageYOffset > animItemOffset - animItemPoint) && pageYOffset < (animItemOffset + animItemHeight)){
                animItem.classList.add('_active');
            } else {
                if(!animItem.classList.contains('anim-no-hide')) {
                    animItem.classList.remove('_active')
                }
            }
        }
    }
    function offset(el){
        const rect = el.getBoundingClientRect(),
            scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
            scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        return {top: rect.top + scrollTop, left: rect.left + scrollLeft}
    }
    setTimeout(() => animOnScroll(), 300)
}
//anim_scroll block end


//modal block start
try {
    const btnOpenModal = document.getElementById('openModal');
    const btnClosedModal = document.getElementById('close_icon');
    const btnsubmit = document.getElementById('closedModal');
    const modal = document.getElementById('modal');
    const userNameInput = document.getElementById('userNameInput');
    const userPhoneInput = document.getElementById('userPhoneInput');

    const open = function (){
        modal.classList.add('modal-active');
        body.classList.add('main_hidden');
    }

    const closed = function (event) {
        event.preventDefault();
        event.stopPropagation();
        sendToTelegram(userNameInput.value, userPhoneInput.value);
        userNameInput.value = "";
        userPhoneInput.value = "";
        modal.classList.remove('modal-active');
        body.classList.remove('main_hidden');
    }
    const exit = function (event) {
        event.preventDefault();
        event.stopPropagation();
        userNameInput.value = "";
        userPhoneInput.value = "";
        modal.classList.remove('modal-active');
        body.classList.remove('main_hidden');
    }
    try{
        const openModalMob = document.getElementById('openModalMob');
        openModalMob.addEventListener('click', open);
    }
    catch (e)
    {
        console.log(e)
    }
    btnOpenModal.addEventListener('click', open);
    btnClosedModal.addEventListener('click', exit);
    btnsubmit.addEventListener('click', closed);
    userPhoneInput.addEventListener('focus', _ => {
        if(!/^\+\d*$/.test(userPhoneInput.value))
            userPhoneInput.value = '+38';

    });

    userPhoneInput.addEventListener('keypress', e => {
        if(!/\d/.test(e.key)) {
            e.preventDefault();
        }
    });


}
catch (e){
    console.log(e)
}
//modal block end


//burger menu block start
const burgerMenuOpenBTN = document.getElementById('burger-menu-open');
const burgerMenu = document.getElementById('burger-menu');
const burgerMenuCloseBTN = document.getElementById('burger-menu-close');
const burgerMenuStickOpen = document.getElementById('open-burger-stick-menu');


const openBurger = () => {
    burgerMenu.classList.add('_active-burger-menu')
}
const closeBurger = () => {
    burgerMenu.classList.remove('_active-burger-menu')
}
window.addEventListener('scroll', closeBurger)
burgerMenuOpenBTN.addEventListener('click', openBurger);
burgerMenuCloseBTN.addEventListener('click', closeBurger);
burgerMenuStickOpen.addEventListener('click', openBurger);
//burger menu block end


//kts block start
try{
    const ktsItems = document.getElementsByClassName('kts_item');
    const ktsArrowLeft = document.getElementById('ktsArrowLeft');
    const ktsArrowRight = document.getElementById('ktsArrowRight');
    const counterBoxPlace = document.getElementById('counterBox');
    let timerId = setInterval(() => stepRight(), 3000);
    if(!counterBoxPlace){
        clearInterval(timerId);
    }

    const idForKtsItems = () =>{
        const couter = ktsItems.length;
        const n = couter;
        const arrCounter = Array.from({length: n}, (_, index) => index + 1);
        for (let i =0; i < ktsItems.length; i++ ){
            ktsItems[i].id = 'ktsItems' + i;
            ktsItems[i].classList.add('invisible');
            ktsItems[0].classList.remove('invisible');
        }
        arrCounter.forEach(i => counterBoxPlace.innerHTML += ` <div class="kts_counter_box">
                <p id="currentBox${i-1}">${i}</p>
            </div>`)
        const counterBox = document.getElementsByClassName('kts_counter_box');
        for (let j = 0; j < counterBox.length; j++){
            counterBox[j].id = 'counterBoxItem' + j;
            counterBox[0].classList.add('_active_kts');
            document.getElementById(`counterBoxItem${j}`).addEventListener('click', setActiveBox)
        }
    }
    const setActiveBox = (e) =>{
        clearInterval(timerId);
        if((e.target.id === ('currentBox'+e.target.id.slice(-1)))||
            (e.target.id === ('counterBoxItem'+e.target.id.slice(-1)))) {
            let act = Number(e.target.id.slice(-1));

        for (let i =0; i < ktsItems.length; i++ ){
            ktsItems[i].classList.add('invisible');

        }
        let activeItem = document.getElementById(`ktsItems${act}`);
        activeItem.classList.remove('invisible');
            setActiveCounter(act);
        }
    }
    const isActive = () => {
        for(let i = 0; i < ktsItems.length; i++){
            if(!ktsItems[i].classList.contains('invisible')){
                return Number(ktsItems[i].id.slice(-1))
            }
        }
    }
    const setActiveCounter = (data) => {
        const counterBox = document.getElementsByClassName('kts_counter_box');
        for(let i = 0; i < counterBox.length; i++ ){
            counterBox[i].classList.remove('_active_kts')
        }
        const activeCounter = document.getElementById(`counterBoxItem${data}`);
        activeCounter.classList.add('_active_kts');
    }

    const stepRightClick = () => {
        stepRight();
        clearInterval(timerId);
    }

    const stepRight = () => {
        let next;
        if(isActive() === (ktsItems.length-1)){
            next = 0;
        }
        else next = +isActive() + 1;
        for (let i =0; i < ktsItems.length; i++ ){
            ktsItems[i].classList.add('invisible');

        }
        setActiveCounter(next);
        let activeItem = document.getElementById(`ktsItems${next}`);
        activeItem.classList.remove('invisible');
    }

    const stepLeft = () => {
        let next;
        if(isActive() === 0){
            next = (ktsItems.length - 1);
        }
        else next = +isActive() - 1;
        for (let i =0; i < ktsItems.length; i++ ){
            ktsItems[i].classList.add('invisible');
        }
        setActiveCounter(next);
        document.getElementById(`ktsItems${next}`).classList.remove('invisible');
        clearInterval(timerId);
    }
    idForKtsItems();

    ktsArrowRight.addEventListener('click',stepRightClick);
    ktsArrowLeft.addEventListener('click',stepLeft);
}
catch (e) {
    console.log(e)
}
//kts block end


//numbers block start
try {
    const numberPlace = document.getElementById('interval');
    const numberPeople = document.getElementById('people');
    const numberCounter = () => {
        for (let i = 0; i <= 1500; i++) {
            setTimeout(()=>{numberPlace.innerHTML = i}, i*1);
        }
        for (let i = 0; i <= 500; i++) {
            setTimeout(()=>{numberPeople.innerHTML = i}, i*3);
        }
    }
    if(numberPlace) {
        numberCounter();
    }
}
catch (e){
    console.log(e)
}

//numbers block end

//telegram block start

    const sendToTelegram = (customer, telephone) => {
        if(telephone.length > 5) {
            const token = '6393648561:AAFFjovMpXylhF2JlPdBtBXhDkd4HST2R60';
            const chatId = '-915443229';
            const url = `https://api.telegram.org/bot${token}/sendMessage?chat_id=${chatId}&text=Имя ${customer}, телефон ${telephone}`;
            const xhttp = new XMLHttpRequest();
            xhttp.open('GET', url, true);
            xhttp.send();
        }
    };

//telegram block end

//footer send block start
    const inputNameFooter = document.getElementById('inputNameFooter');
    const inputTelFooter = document.getElementById('inputTelFooter');
    const inputBtnFooter = document.getElementById('inputBtnFooter');
    const sendFooterData = function (event) {
        event.preventDefault();
        event.stopPropagation();
        sendToTelegram(inputNameFooter.value, inputTelFooter.value);
        inputNameFooter.value = "";
        inputTelFooter.value = "";
    }
    inputTelFooter.addEventListener('focus', _ => {
        if(!/^\+\d*$/.test(inputTelFooter.value))
            inputTelFooter.value = '+38';

    });
    inputTelFooter.addEventListener('keypress', e => {
        if(!/\d/.test(e.key)) {
            e.preventDefault();
        }
    });
    inputBtnFooter.addEventListener('click', sendFooterData);
//footer send block end


